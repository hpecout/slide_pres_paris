# Présentation séminaire P.A.R.I.S (23/09/2022)

## Géomatique R et Notebook : à la racherche de la reproductibilité

Ingénieur en science de l'information géographique au CNRS depuis 10 ans, j'ai peu à peu converti mes compétences en SIG vers la programmation sous R. Cela m'a permis d’outrepasser le périmètre de la géomatique en m'initiant par exemple à l'analyse de réseau, l'analyse textuelle ou encore au web scraping. Cela m'a également amené à découvrir le format de publication notebook permettant d’entremêler du langage naturel et du langage informatique et qui, combiné au paradigme de la programmation lettrée et à l’utilisation de Git, offre un socle efficient en matière de science ouverte et de reproductible. Je tente aujourd’hui de transmettre mes connaissances accumulées dans ce domaine dans le cadre de mes collaborations, de formations ou via le projet [Rzine](https://rzine.fr/) qui offre un espace de publication au format notebook.

➡ [Consulter le diaporama](https://hpecout.gitpages.huma-num.fr/slide_pres_paris) (CC BY-SA 4.0)

[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)
